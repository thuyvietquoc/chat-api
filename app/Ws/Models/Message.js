const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    room_id: {type: String, required: true, index: true},
    message_id: {type: String, required: true, index: true},
    message_type: {type: String, enum: ['text', 'file', 'video', 'image'], default: 'text'},
    text: {type: String, validate: {
            validator: function(v) {
                return v.length > 0;
            },
            message: props => `TEXT_IS_REQUIRED`
        }},
    user: {
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    seen: {type: Object},
    received: {type: Object},
    createdAt: {type: Date},
    updatedAt: {type: Date}
});

const Message = mongoose.model("ChatMessages", MessageSchema, 'chat_messages');

module.exports = Message;
