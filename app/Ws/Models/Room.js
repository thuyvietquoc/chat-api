const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ChatRoomScheme = new Schema({
    room_id: {type: String, required: true, index: true},
    name: {type: String, default: ''},
    room_type: {type: String, enum: ['single', 'group'], default: 'single'},
    user_block_notify: {
        type: Array,
        default: []
    },
    users: {
        type: Object,
    },
    privacy: {type: String, enum: ['public', 'private'], default: 'public'}
}, { timestamps: true });

const ChatRoom = mongoose.model("ChatRooms", ChatRoomScheme, 'chat_rooms');

module.exports = ChatRoom;
