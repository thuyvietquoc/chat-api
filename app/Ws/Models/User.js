const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {type: String},
    avatar: {type: String}
}, { timestamps: true });

const Users = mongoose.model("Users", UserSchema, 'users');

module.exports = Users;
