module.exports = {
    generate_random_string: (length = 32) =>{
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(let i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },
    pagination: (query) => {
        let page  = parseInt(query.page) - 1 || 0;
        let limit = parseInt(query.limit) || 10;

        return {
            page  : page < 0 ? 0 : page,
            limit : limit < 0 ? 10 : limit > 100 ? 100 : limit
        }
    },
    arrFlatten(arr) {
        return [].concat(...arr.map(v => (Array.isArray(v) ? this.arrFlatten(v) : v)))
    }
};