const User         = require('../Models/User');
const WS_CONSTANTS = require('../Constants');

class SocketAuth {
    constructor(nsp, io) {
        this.nsp    = nsp;
        this.io     = io;
    }

    Authentication () {
        this.io.use( async (socket, next) => {
            try {
                let query   = socket.handshake.query;
                let user_id = query.token.replace('Bearer ', '');
                let user    = await User.findOne({ _id : user_id })
                                        .select(['name', 'avatar']);

                if (!user) {
                    throw new Error(WS_CONSTANTS.error_message.USER_NOT_FOUND)
                }

                socket.request.user = user;
                next();
            } catch (error) {
                console.log(error);
                next(error)
            }
        })
    }
}

module.exports = SocketAuth;