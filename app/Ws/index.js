// TODO: SIMPLE EMBEDDED CHAT INSTANCE

module.exports = (server) => {
    // Caches
    /**
     * Cache structure { userId: Socket Instance }
     * @type {{}}
     */
    global.clients = {}; // Only contain _id user as key
    /**
     * Cache structure { roomid : [messages] }
     * @type {{}}
     */
    global.messages = {}; // See message model - roomId as key = [Array of messages]
    global.roomBulk = []; // Bulk write operator for room
    global.messageBulk = []; // Bulk write operator for message
    // SocketIO
    const io   = require('socket.io')(server);
    const chat = io.of('/chat');
    const ChatController = require('./Controllers/ChatController');

    const SocketAuth = require('./Middlewares/Auth');
    console.log('SOCKET CREATED');
    // Authenticate
    const socketAuth = new SocketAuth(chat, io);
    socketAuth.Authentication();

    chat.on('connection', socket => {
        const chatController = new ChatController(socket, chat);
        chatController.onConnect();
        chatController.onAction();
        chatController.onDisconnect();
    });

    // Message insert schedule.
    const insertToDatabase = async () => {
        const Message  = require('./Models/Message');
        console.log('list client = ', Object.values(clients).length);
        try {
            let response;
            if(messageBulk.length) {
                response = await Message.bulkWrite(messageBulk);
                // reset message operators & cache
                global.messageBulk = [];
                global.messages = {};
            }
            console.log(response);
        } catch (e) {
            console.log(e);
        }
    };

    const gracefulShutdown = async () => {
        await insertToDatabase();
        process.exit();
    };

    const CronJob = require('cron').CronJob;

    const job = new CronJob('0 */15 * * * *', function() {
        insertToDatabase();
    });
    job.start();

    // listen for TERM signal .e.g. kill
    process.on ('SIGTERM', gracefulShutdown);

    // listen for INT signal e.g. Ctrl-C
    process.on ('SIGINT', gracefulShutdown);
};