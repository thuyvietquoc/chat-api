const WS_CONSTANTS      = require('../Constants');
const MessageController = require('../Controllers/MessageController.js');
const RoomController    = require('../Controllers/RoomController.js');

class ChatController {
    constructor(socket, nsp) {
        this.socket = socket;
        this.nsp = nsp;
        this.roomController = new RoomController(socket, nsp);
    }

    onConnect () {
        console.log('user connected - ', this.socket.request.user._id);
        // Save client to cache
        clients[this.socket.request.user._id] = this.socket;
    }

    onAction () {
        this.socket.on('action', (action, ack = () => {}) => {
            console.log(action);
            switch (action.type) {
                case WS_CONSTANTS.action_types.FETCH_ROOM_MESSAGE: {
                    let {room_id = null, cursor = new Date().toISOString(), page = 1, limit = 10} = action.payload || {};
                    let messageController = new MessageController(this.socket, this.nsp, {
                        room_id
                    });
                    return messageController.fetchRoomMessage({limit, cursor, ack})
                }
                case WS_CONSTANTS.action_types.SEND_MESSAGE_TO_ROOM: {
                    let { room_id, text = '' } = action.payload || {};
                    let messageController = new MessageController(this.socket, this.nsp, {
                        user: this.socket.request.user,
                        text,
                        room_id
                    });
                    return messageController.sendMessageToRoom({ack});
                }
                case WS_CONSTANTS.action_types.JOIN_ONE_TO_ONE: {
                    let { user_id = null } = action.payload || {};

                    return this.roomController.joinOneToOne({user_id, ack})
                }
                case WS_CONSTANTS.action_types.USER_JOIN_ROOM: {
                    let { room_id } = action.payload || {};
                    return this.roomController.joinUserRoom({room_id}, ack);
                }
                case WS_CONSTANTS.action_types.USER_LEAVE_ROOM: {
                    let { room_id } = action.payload || {};
                    return this.roomController.leaveRoom({room_id, ack});
                }
                case WS_CONSTANTS.action_types.DELETE_MESSAGE: {
                    let { room_id, message_id } = action.payload || {};

                    if(!room_id || !message_id) return;
                    let messageController = new MessageController(this.socket, this.nsp, {});
                    return messageController.removeMessage({room_id, message_id});
                }
                case WS_CONSTANTS.action_types.CREATE_ROOM: {
                    let {user_ids = [], room_name = ''} = action.payload || {};

                    if(!user_ids.length) return ack({error: WS_CONSTANTS.error_message.CANNOT_CREATE_ROOM})

                    return this.roomController.createGroup({room_name, user_ids, ack})
                }
                case WS_CONSTANTS.action_types.CHANGE_ROOM_NAME: {
                    let { room_id, room_name } = action.payload || {};
                    if(!room_id) return;

                    return this.roomController.changeRoomName({room_id, room_name, ack});
                }
                case WS_CONSTANTS.action_types.ADD_USER_TO_GROUP: {
                    let {room_id, user_ids, user_names = []} = action.payload || {};
                    if(!room_id) return;

                    this.roomController.addUsersToGroup({room_id, user_ids, ack});
                    const messageController = new MessageController(this.socket, this.nsp, {});

                    for(let user_name of user_names) {
                        messageController.sendSystemMessage({
                            room_id,
                            message: {
                                text: user_name,
                                createdAt: new Date().toISOString()
                            }
                        })
                    }
                    return;
                }
                case WS_CONSTANTS.action_types.FETCH_USER_IN_ROOM: {
                    let {room_id} = action.payload || {};
                    if(!room_id) return;

                    return this.roomController.fetchUserInRoom({room_id, ack});
                }
                case WS_CONSTANTS.action_types.SEND_BOT_MESSAGE: {
                    let { text = '' } = action.payload || "";

                    let botController = new BotController(this.socket, this.nsp);
                    botController.botReply({text, ack});
                }
                default: return null;
            }
        })
    }

    onDisconnect() {
        this.socket.on('disconnect', () => {
            console.log('user disconnected - ', this.socket.request.user._id);
            // Remove user from cache
            delete clients[this.socket.request.user._id];
        })
    }
}

module.exports = ChatController;