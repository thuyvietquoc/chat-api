const User           = require('../Models/User');
const ChatRoom       = require('../Models/Room');
const Message        = require('../Models/Message');
const WS_CONSTANTS   = require('../Constants');
const Helper         = require('../Utils/Helper');

class RoomController {
    constructor(socket, nsp) {
        this.socket  = socket;
        this.nsp     = nsp;
    }

    /**
     * Fetch all room of users
     * @param user
     * @param page
     * @param limit
     * @returns {Promise<*|Array>}
     */
    async fetchUserRooms ({user, page = 1, limit = 10}) {
        let pagination   = Helper.pagination({page, limit});
        let room_ids  = Object.keys(global.messages);
        let result = [], numberOfPage, rooms = [];

        let cachedRoom = [];

        try {
            cachedRoom = await ChatRoom.find({
                $or: [
                    {room_id: {$in: room_ids}}
                ],
                [`users.${user._id.toString()}`]: true
            }).skip(pagination.page * pagination.limit).limit(pagination.limit)
                .select('room_id name users room_type').lean();

            cachedRoom = cachedRoom.map(room => {
                let room_messages = (global.messages[room.room_id] || []);

                return {
                    ...room,
                    message: room_messages[room_messages.length - 1] || null
                }
            });
        } catch (e) {
            console.log(e);
        }

        result = result.concat(cachedRoom);

        numberOfPage = Math.floor(result.length / pagination.limit);

        if(cachedRoom.length < limit) {
            pagination   = Helper.pagination({page: page - numberOfPage, limit});
            let cacheRoomIds = cachedRoom.map(room => room.room_id);

            rooms = await Message.aggregate([
                {$sort: {createdAt: -1}},
                {$lookup: {
                        from: 'chat_rooms',
                        localField: 'room_id',
                        foreignField: 'room_id',
                        as: 'room'}
                },
                {$unwind: '$room'},
                {$match: {room_id: {$nin: cacheRoomIds}, [`room.users.${user._id.toString()}`]: true} },
                {$group: {
                        _id: '$room._id',
                        room_type: {$first: '$room.room_type'},
                        room_id: {$first: '$room.room_id'},
                        name: {$first: '$room.name'},
                        message: {
                            $first: {
                                text: '$text',
                                createdAt: '$createdAt'
                            }
                        },
                        users: {$first: '$room.users'}
                    }
                },
                {$skip: pagination.page * pagination.limit},
                {$limit: pagination.limit}
            ]).exec();
            result = result.concat(rooms);
        }

        // Fetch room users
        let users = result.map(room => Object.keys(room.users));
        // Flatten users
        users = new Set( Helper.arrFlatten(users) );
        users.delete(user._id.toString()); // Remove current user for showing other user in rooms.
        users  = await User.find({_id: {$in: [...users]}}).exec();

        result = result.reduce((res, data) => {
            let user = users.find(user_item => data.users[user_item._id.toString()]);

            if(!!user) {
                res.push({
                    ...data,
                    user
                })
            }
            return res
        }, []);

        result = result.sort(function(a, b) {
            a = !!a.message ? a.message : {};
            b = !!b.message ? b.message : {};
            let left_date  = !!a.createdAt ? new Date(a.createdAt).getTime() : null;
            let right_date = !!b.createdAt ? new Date(b.createdAt).getTime() : null;
            return (a===null)-(b===null) || -(left_date > right_date) || +(left_date < right_date);
        });

        return result;
    }

    /**
     * Chat one to one
     * @param user_id
     * @param ack
     * @returns {Promise<null>}
     */
    async joinOneToOne ({user_id = null, ack = () => {}}) {
        let currentUser = this.socket.request.user;

        if(!user_id) return null;

        // Prevent users chat by their own
        if(currentUser._id.toString() === user_id) return;

        let receiver = await User.findOne({
            _id: user_id,
        }).select('avatar name first_name last_name').exec();

        if(!receiver)
            return ack({error: WS_CONSTANTS.error_message.USER_INACTIVE});

        let room = await ChatRoom.findOne({$and: [
                {[`users.${currentUser._id.toString()}`]: true},
                {[`users.${user_id}`]: true},
                {room_type: 'single'}
            ]}).exec();

        if(!room) {
            room = {
                room_id: Helper.generate_random_string(),
                users: {
                    [user_id]: true,
                    [currentUser._id.toString()]: true
                },
                room_type: 'single',
                user_block_notify: [],

            };
            // Save room to db
            try {
                await ChatRoom.update({room_id: room.room_id}, {$set: room}, {upsert: true});
            } catch (e) {
                console.log(e.stack);
                return ack({
                    error: WS_CONSTANTS.error_message.CANNOT_CREATE_ROOM
                })
            }
        }
        // Both user join room
        this.socket.join(room.room_id);
        console.log('ROOM ID = ', room.room_id);
        // Send room id to joiner
        if(!!ack) {
            ack({
                room_id: room.room_id
            })
        }
    }

    /**
     * Create group chat
     * @param room_name
     * @param user_ids
     * @param ack
     * @returns {Promise<*>}
     */
    async createGroup ({room_name = '', user_ids, ack}) {
        let currentUser = this.socket.request.user;
        let users = { [currentUser._id.toString()]: true };

        user_ids.forEach(user_id => {
            users = {
                ...users,
                [user_id]: true
            }
        });

        let room = {
            name: room_name,
            room_id: Helper.generate_random_string(),
            room_type: 'group',
            users
        };

        try {
            room = await ChatRoom.findOneAndUpdate({room_id: room.room_id}, {$set: room}, {upsert: true, new: true});
        } catch (e) {
            console.log(e.stack);
            return ack({error: WS_CONSTANTS.error_message.CANNOT_CREATE_ROOM});
        }

        return ack({
            room_id: room.room_id
        })
    }

    /**
     * Change group chat name
     * @param room_id
     * @param room_name
     * @param ack
     * @returns {Promise<void>}
     */
    async changeRoomName ({room_id, room_name = '', ack = () => {}}) {
        if(!room_name || !room_name.length) return ack({error: WS_CONSTANTS.validate_message.ROOM_NAME_REQUIRE});

        let currentUser = this.socket.request.user;
        let room = await ChatRoom.findOneAndUpdate({
            room_id,
            [`users.${currentUser._id.toString}`]: true
        }, {
            $set: {
                name: room_name
            }
        }, {new:true}).lean();

        if(!room) return ack({
            error: WS_CONSTANTS.error_message.ROOM_NOT_FOUND
        });

        return ack({success:true, data: room});
    }


    /**
     * Add specific user to group
     * NOTE: Only user in room can add other user to group
     * @param room_id
     * @param user_ids
     * @param ack
     * @returns {Promise<void>}
     */
    async addUsersToGroup ({room_id = null, user_ids = [], ack = () => {}}) {
        let users = {};
        let room  = null;
        let currentUser = this.socket.request.user;

        user_ids.forEach(user_id => {
            users = {
                ...users,
                [`users.${user_id}`]: true
            }
        });

        try {
            room = await ChatRoom.findOneAndUpdate({
                room_id: room_id,
                [`users.${currentUser._id.toString()}`]: true
            }, {
                $set: users
            }, {new: true}).lean();

            if(!room) throw new Error(WS_CONSTANTS.error_message.CANNOT_ADD_USER_TO_ROOM);

        } catch (e) {
            return ack({error: e.message});
        }
        ack({success: true, data: room})
    }

    async fetchUserInRoom ({room_id = null, ack = () => {}}) {
        let room   = await ChatRoom.findOne({room_id}).select('users').lean();

        if(!room) return ack({error: WS_CONSTANTS.error_message.ROOM_NOT_FOUND});

        let users  = Object.keys(room.users);
        users = await User.find({_id: {$in: users}}).select(User.fillable).lean();

        return ack({data: users});
    }

    /**
     * case 1: User will join to chat room when they receive message push notification
     * case 2: User can manual join to a chat room
     * @param room_id
     * @param ack
     * @returns {Promise<void>}
     */
    async joinUserRoom ({room_id}, ack) {
        if(!room_id) return;
        let currentRoom = await ChatRoom.findOne({room_id: room_id}).exec();

        if(!currentRoom) return ack({error: WS_CONSTANTS.error_message.ROOM_NOT_FOUND});

        let currentUser = this.socket.request.user;
        // Validate User
        if(!currentRoom.users[currentUser._id.toString()]) return ack({error: WS_CONSTANTS.error_message.UNAUTHORIZED});

        clients[currentUser._id].join(room_id);
        await ChatRoom.update({room_id: room_id}, {$set: {
                [`users.${currentUser._id.toString()}`]: true}
        }, {upsert: true}).exec();
        // response
        if(!!ack) {
            return ack({
                success: true
            })
        }
    }

    async leaveRoom ({room_id, ack}) {
        if(!room_id) return;

        let currentUser = this.socket.request.user;
        clients[currentUser._id].leave(room_id);
        await ChatRoom.update({room_id: room_id}, {$unset: {
                [`users.${currentUser._id.toString()}`]: true
            }}).exec();
        // response
        if(!!ack) {
            return ack({
                success: true
            })
        }
    }
}

module.exports = RoomController;