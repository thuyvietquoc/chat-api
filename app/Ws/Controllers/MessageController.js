const WS_CONSTANTS   = require('../Constants');
const Messages       = require('../Models/Message');
const ChatRoom       = require('../Models/Room');
const Helper         = require('../Utils/Helper');

class MessageController {
    constructor(socket, nsp, {text = '', user = null, room_id = null}) {
        this.socket = socket;
        this.nsp    = nsp;
        // Chat content
        this.room_id = room_id;
        this.text   = text;
        this.user   = user;
    }

    /**
     * Fetch message with room_id
     * Paginate with date cursor
     * @param cursor
     * @param limit
     * @param ack
     * @returns {Promise<Array>}
     */
    async fetchRoomMessage({cursor = new Date().toISOString(), limit = 10, ack = () => {}}) {
        if(!this.room_id) return [];
        let result = [];

        try {
            let currentRoom = await ChatRoom.findOne({room_id: this.room_id}).exec();

            if(!currentRoom) return ack({error: WS_CONSTANTS.error_message.ROOM_NOT_FOUND});

            // Check if user not in room then fetch messages
            if(!currentRoom.users[this.socket.request.user._id]) {
                // response
                return ack({error: WS_CONSTANTS.error_message.UNAUTHORIZED});
            }

            let cacheMessage  = messages[this.room_id] || [];
            // Fetch cache
            cacheMessage  = cacheMessage.reduce((result, message) => {
                if( new Date(message.createdAt).getTime() < new Date(cursor).getTime()) {
                    result.push({
                        ...message,
                        _id: message.message_id
                    })
                }
                return result;
            }, []);
            // Sort
            cacheMessage  = cacheMessage.sort((a,b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime());
            cacheMessage = cacheMessage.slice(0, limit);

            let messageDB = [];

            if(cacheMessage.length < limit) {
                messageDB = await Messages
                    .find({room_id: this.room_id, createdAt: {$lt: new Date(cursor).toISOString()}})
                    .populate([{
                        path: 'user',
                        select: '_id first_name last_name avatar status privacy',
                        model: 'Users'
                    }]).sort({createdAt: -1}).limit(limit).exec();
            }
            result = cacheMessage.concat(messageDB);
        } catch (e) {
            result = [];
            console.log(e.stack);
        }
        // Response
        return ack({
            messages: result,
            limit
        })
    }

    /**
     * DEPRECATED
     * @param room_id
     * @param message_id
     */
    removeMessage({room_id, message_id}) {
        // Remove message from cache
        messages[room_id] = messages[room_id].filter(message => message.message_id !== message_id);
        // Add remove operator
        messageBulk.push({
            deleteOne : { filter: { message_id } }
        });
    }

    /**
     * Send Push notification messages
     * @param message
     * @returns {Promise<void>}
     */
    async sendPushMessage ({message}) {
        try {
            let room = await ChatRoom.findOne({room_id: this.room_id}).select('users').exec();
            if(!room) return;

            let users   = new Set(Object.keys(room.users));
            let usersOnline = Object.keys(global.clients);

            // Remove sending push notification for online users
            for(let userOnline of usersOnline) {
                users.delete(userOnline.toString());
            }

            users.delete(this.socket.request.user._id.toString());

            // TODO: SEND PUSH NOTIFICATION FOR OFFLINE USER OR USER NOT IN ROOM

        } catch (e) {
            console.log(e.stack);
        }
    }

    /**
     * Send messages to specific room
     * Save message to cache.
     * @param ack
     * @returns {Promise<void>}
     */
    async sendMessageToRoom ({ack = () => {}}) {
        if(!this.room_id || !this.text.length) return;

        let message = {
            room_id: this.room_id,
            message_id: Helper.generate_random_string(),
            text: this.text,
            user: this.user,
            message_type: 'text',
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString()
        };

        try {
            // Initial cache
            if(!messages[this.room_id]) {
                messages[this.room_id] = [];
            }
            // Save to cache
            messages[this.room_id] = [...messages[this.room_id], message];
            // Save to bulk
            messageBulk.push({
                updateOne: {
                    filter: {room_id: this.room_id, message_id: message.message_id},
                    update: {$set: {
                            ...message,
                            text: message.text,
                            user: message.user._id,
                            message_type: message.message_type,
                            createdAt: message.createdAt,
                            updatedAt: message.updatedAt
                        }},
                    upsert: true
                }
            });
        } catch (e) {
            console.log(e.stack);
            ack({
                error: WS_CONSTANTS.error_message.CANNOT_SEND_MESSAGE
            })
        }
        // Push notification.
        let pushMessage = {
            contents: {en: message.text},
            headings: {en: `${this.user.last_name} ${this.user.first_name}`},
            data: {
                room_id: this.room_id,
                message: {
                    ...message,
                    _id: message.message_id
                }
            }
        };

        this.sendPushMessage({message: pushMessage});
        // Emit to all user in room_id.
        this.socket.to(this.room_id).emit('action', {
            type: WS_CONSTANTS.action_types.SERVER_SEND_MESSAGE,
            payload: {
                ...message,
                _id: message.message_id
            }
        });
        ack({
            success: true
        })
    }

    /**
     * DEPRECATED
     * @param room_id
     * @param message
     * @param ack
     * @returns {Promise<void>}
     */
    async sendSystemMessage({room_id = null, message, ack = () => {}}) {
        this.socket.emit('action', {
            type: WS_CONSTANTS.action_types.SERVER_SEND_MESSAGE,
            payload: {
                ...message,
                _id: message.message_id,
                isSystem: true
            }
        })
    }
}

module.exports = MessageController;